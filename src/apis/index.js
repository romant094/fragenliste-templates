import { api, authApi } from '../services'

const {
  REACT_APP_AUTH_LOGIN,
  REACT_APP_AUTH_PASSWORD
} = process.env

const headers = () => {
  return {
    headers: {
      'authorization': `Bearer ${localStorage.getItem('token') || ''}`
    }
  }
}

export const getTemplates = async () => {
  return await api.get({
    endpoint: '/templates',
    params: { ...headers() },
  })
}

export const deleteTemplate = async id => {
  return await api.delete({
    endpoint: `/templates/${id}`,
  })
}

export const updateTemplate = async (id, data) => {
  return await api.patch({
    endpoint: `/templates/${id}`,
    data,
  })
}

export const createTemplate = async data => {
  return await api.post({
    endpoint: `/templates`,
    data,
  })
}

export const login = async () => {
  const { data } = await authApi.post({
    endpoint: `/login`,
    data: {
      application: 2,
      email: REACT_APP_AUTH_LOGIN,
      emailErrorMessage: '',
      emailHasError: false,
      password: REACT_APP_AUTH_PASSWORD,
      passwordErrorMessage: '',
      passwordHasError: false,
    },
  })
  return data.access_token
}
