import React, { useEffect, useState } from 'react'
import { useParams, Link, Redirect } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faArrowUp, faArrowDown, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { CopyBlock, dracula } from 'react-code-blocks'
import { createTemplate, updateTemplate } from '../../apis'

const COMPONENT_TEMPLATE = { component: '', header: true, footer: true, sidebar: true }
const SECTION_TEMPLATE = { section: '', submenu: [] }

const isCheckbox = target => target.hasOwnProperty('checked')

export const Template = ({
  templates,
}) => {
  const { id } = useParams()
  const [redirect, setRedirect] = useState(false)
  const [template, setTemplate] = useState({
    name: '',
    menu_config: [],
    is_default: 0
  })
  const { menu_config: config, name } = template

  useEffect(() => {
    window.scrollTo(0, 0)
    if (id && templates.length > 0) {
      const template = templates.find(item => item.id === +id)
      setTemplate(template)
    }
  }, [templates, id])

  const setComponentsSort = () => {
    let count = 1
    return {
      ...template,
      menu_config: config.map(configItem => {
        const { submenu } = configItem
        return {
          ...configItem,
          submenu: submenu.map(item => {
            return {
              ...item,
              sort: count++,
            }
          }),
        }
      }),
    }
  }

  const handleChangeField = event => {
    const { name, value, checked } = event?.target

    setTemplate(prev => ({
      ...prev,
      [name]: isCheckbox(event.target) ? +checked : value,
    }))
  }


  const handleChangeSectionName = (event, index) => {
    const configItem = config[index]
    setTemplate(prev => ({
      ...prev,
      menu_config: [
        ...config.slice(0, index),
        {
          ...configItem,
          section: event.target.value,
        },
        ...config.slice(index + 1),
      ],
    }))
  }

  const handleChangeComponentField = (event, index, idx) => {
    const { name, value, checked } = event.target

    const configItem = config[index]
    const { submenu } = configItem
    const submenuItem = submenu[idx]
    setTemplate(prev => ({
      ...prev,
      menu_config: [
        ...config.slice(0, index),
        {
          ...configItem,
          submenu: [
            ...submenu.slice(0, idx),
            {
              ...submenuItem,
              [name]: isCheckbox(event.target) ? checked : value,
            },
            ...submenu.slice(idx + 1),
          ],
        },
        ...config.slice(index + 1),
      ],
    }))
  }

  const handleMoveComponent = (index, idx, delta) => {
    const configItem = config[index]
    const { submenu } = configItem
    const submenuItem = submenu[idx]
    const submenuItemToChangeWith = submenu[idx + delta]
    let changedSubmenu

    if (delta === -1) {
      changedSubmenu = [
        ...submenu.slice(0, idx + delta),
        submenuItem,
        submenuItemToChangeWith,
        ...submenu.slice(idx + 1),
      ]
    } else {
      changedSubmenu = [
        ...submenu.slice(0, idx),
        submenuItemToChangeWith,
        submenuItem,
        ...submenu.slice(idx + delta + 1),
      ]
    }

    setTemplate(prev => ({
      ...prev,
      menu_config: [
        ...config.slice(0, index),
        {
          ...configItem,
          submenu: changedSubmenu,
        },
        ...config.slice(index + 1),
      ],
    }))
  }

  const deleteComponent = (index, idx) => {
    const configItem = config[index]
    const { submenu } = configItem
    setTemplate(prev => ({
      ...prev,
      menu_config: [
        ...config.slice(0, index),
        {
          ...configItem,
          submenu: [
            ...submenu.slice(0, idx),
            ...submenu.slice(idx + 1),
          ],
        },
        ...config.slice(index + 1),
      ],
    }))
  }

  const handleAddComponent = index => {
    const configItem = config[index]
    const { submenu } = configItem
    setTemplate(prev => ({
      ...prev,
      menu_config: [
        ...config.slice(0, index),
        {
          ...configItem,
          submenu: [
            ...submenu,
            COMPONENT_TEMPLATE,
          ],
        },
        ...config.slice(index + 1),
      ],
    }))
  }

  const handleAddSection = () => {
    setTemplate(prev => ({
      ...prev,
      menu_config: [
        ...config,
        SECTION_TEMPLATE,
      ],
    }))
  }

  const handleDeleteSection = index => {
    setTemplate(prev => ({
      ...prev,
      menu_config: [
        ...config.slice(0, index),
        ...config.slice(index + 1),
      ],
    }))
  }

  const handleMoveSection = (index, delta) => {
    const configItem = config[index]
    const configItemToChangeWith = config[index + delta]

    let updated

    if (delta === -1) {
      updated = [
        ...config.slice(0, index + delta),
        configItem,
        configItemToChangeWith,
        ...config.slice(index + 1),
      ]
    } else {
      updated = [
        ...config.slice(0, index),
        configItemToChangeWith,
        configItem,
        ...config.slice(index + delta + 1),
      ]
    }

    setTemplate(prev => ({
      ...prev,
      menu_config: updated,
    }))
  }

  const handleSubmitData = async () => {
    const template = setComponentsSort()
    try {
      if (id) {
        await updateTemplate(id, template)
      } else {
        await createTemplate(template)
      }
      await setRedirect(true)
    } catch (error) {
      console.error(error)
    }
  }

  if (redirect) {
    return <Redirect to='/' />
  }

  return (
    <>
      <header className='col-12'>
        <h2 className='mb-3'>{id ? `Template ${id} - ${template.name}` : 'Create new template'}</h2>
        <div>
          <Link
            to='/'
            className='btn btn-sm btn-link'
          >
            <FontAwesomeIcon icon={faArrowLeft} />&nbsp;&nbsp;Back
          </Link>
        </div>
      </header>
      <main className='d-flex flex-row pb-5'>
        <div className='col-6 template-form'>
          <div className='mb-5'>
            <div className='form-group mb-3'>
              <label htmlFor='name'>Template name</label>
              <input
                type='text'
                className='form-control'
                id='name'
                placeholder='Template name'
                name='name'
                value={name}
                onChange={handleChangeField}
              />
            </div>
            <div className='form-group form-check'>
              <input
                type='checkbox'
                className='form-check-input'
                id='default'
                name='is_default'
                checked={template.is_default}
                onChange={handleChangeField}
              />
              <label
                className='form-check-label'
                htmlFor='default'
              >
                Default template <strong>(only one should be default)</strong>
              </label>
            </div>
          </div>
          <div className='mb-5'>
            {config.map((
              { section, submenu }, index) => (
              <div
                className={`${index !== config.length - 1 && 'mb-5'}`}
                key={index}
              >
                <div className='row mb-5'>
                  <div className='col-1 d-flex flex-column'>
                    <button
                      className='btn btn-sm btn-outline-primary mb-2'
                      onClick={() => handleMoveSection(index, -1)}
                      disabled={index === 0}
                    >
                      <FontAwesomeIcon icon={faArrowUp} />
                    </button>
                    <button
                      className='btn btn-sm btn-outline-primary'
                      onClick={() => handleMoveSection(index, 1)}
                      disabled={index === config.length - 1}
                    >
                      <FontAwesomeIcon icon={faArrowDown} />
                    </button>
                  </div>
                  <div className='col-10'>
                    <div className='form-group mb-0'>
                      <label htmlFor={`section${index}`}>Section name</label>
                      <input
                        type='text'
                        className='form-control'
                        id={`section${index}`}
                        placeholder='Section name'
                        name='section'
                        value={section || ''}
                        onChange={event => handleChangeSectionName(event, index)}
                      />
                    </div>
                  </div>
                  <div className='col-1 d-flex align-items-stretch'>
                    <button
                      className='btn btn-sm btn-outline-danger'
                      onClick={() => handleDeleteSection(index)}
                    >
                      <FontAwesomeIcon icon={faTrashAlt} />
                    </button>
                  </div>
                </div>
                <table className='table table-striped'>
                  <thead>
                  <tr>
                    <th />
                    <th>order</th>
                    <th>component</th>
                    <th>header</th>
                    <th>footer</th>
                    <th>sidebar</th>
                    <th />
                  </tr>
                  </thead>
                  <tbody>
                  {submenu?.length > 0 && submenu.map(({
                    component,
                    header,
                    footer,
                    sidebar,
                  }, idx) => (
                    <tr key={idx}>
                      <td className='d-flex align-items-center'>
                        <FontAwesomeIcon
                          className={`mr-2 text-primary cursor-pointer ${idx === 0 && 'disabled'}`}
                          icon={faArrowUp}
                          onClick={() => handleMoveComponent(index, idx, -1)}
                        />
                        <FontAwesomeIcon
                          className={`text-primary cursor-pointer ${idx === submenu.length - 1 && 'disabled'}`}
                          icon={faArrowDown}
                          onClick={() => handleMoveComponent(index, idx, 1)}
                        />
                      </td>
                      <td>{idx + 1}</td>
                      <td>
                        <input
                          type='text'
                          className='form-control fz-14'
                          id={`component-${index}-${idx}`}
                          placeholder='Component name'
                          name='component'
                          value={component || ''}
                          onChange={event => handleChangeComponentField(event, index, idx)}
                        />
                      </td>
                      <td>
                        <input
                          type='checkbox'
                          className='form-control fz-2'
                          name='header'
                          checked={header}
                          onChange={event => handleChangeComponentField(event, index, idx)}
                        />
                      </td>
                      <td>
                        <input
                          type='checkbox'
                          className='form-control fz-2'
                          name='footer'
                          checked={footer}
                          onChange={event => handleChangeComponentField(event, index, idx)}
                        />
                      </td>
                      <td>
                        <input
                          type='checkbox'
                          className='form-control fz-2'
                          name='sidebar'
                          checked={sidebar}
                          onChange={event => handleChangeComponentField(event, index, idx)}
                        />
                      </td>
                      <td>
                        <FontAwesomeIcon
                          icon={faTrashAlt}
                          className='text-danger cursor-pointer'
                          onClick={() => deleteComponent(index, idx)}
                        />
                      </td>
                    </tr>
                  ))}
                  <tr>
                    <td colSpan={4}>
                      <button
                        className='btn btn-primary btn-sm'
                        onClick={() => handleAddComponent(index)}
                      >
                        Add component
                      </button>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
            ))}
            <button
              className='btn btn-primary my-3'
              onClick={handleAddSection}
            >
              Add section
            </button>
          </div>
          <div className='d-flex'>
            <Link
              to='/'
              className='btn btn-outline-danger mr-3'
            >
              Cancel
            </Link>
            <button
              className='btn btn-primary'
              onClick={handleSubmitData}
            >
              Submit
            </button>
          </div>
        </div>
        <div className='col-6'>
          <div className='result-code'>
            <CopyBlock
              language='json'
              text={JSON.stringify(template)}
              theme={dracula}
              codeBlock
            />
            <button
              className='btn btn-outline-primary btn-sm mt-2'
              onClick={() => {
                console.log(template)
              }}
            >
              Show object in console
            </button>
          </div>
        </div>
      </main>
    </>
  )
}
