import React, { useState, useEffect } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { Loader } from '../Loader'
import { TemplateList } from '../TemplateList'
import { Template } from '../Template'
import * as api from '../../apis'

export const App = () => {
  const [templates, setTemplates] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)

  useEffect(() => {
    getTemplates()
  }, [])

  const getToken = async () => {
    const token = localStorage.getItem('token') || await api.login()
    localStorage.setItem('token', token)
  }

  const getTemplates = async () => {
    setLoading(true)
    setError(false)
    try {
      const data = await api.getTemplates()
      if (!data) {
        throw new Error('Error on loading data')
      }
      setTemplates(data)
      setLoading(false)
    } catch (err) {
      setLoading(false)
      setError(true)
    }
  }

  const deleteTemplate = async id => {
    await api.deleteTemplate(id)
    await getTemplates()
  }

  if (loading && !templates?.length) {
    return <Loader />
  }

  return (
    <div className='container-fluid'>
      <Switch>
        <Route
          path='/'
          exact
          render={() => (
            <TemplateList
              templates={templates}
              error={error}
              deleteTemplate={deleteTemplate}
              setTemplates={setTemplates}
              getTemplates={getTemplates}
            />
          )
          }
        />
        <Route
          path='/template/:id'
          render={() => (
            <Template
              key={Math.random()}
              templates={templates}
            />
          )}
        />
        <Route
          path='/create-template'
          render={() => <Template />}
        />
        <Route path='*'>
          <Redirect to='/' />
        </Route>
      </Switch>
    </div>
  )
}
