import React, { useState, useMemo } from 'react'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons'
// import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

export const TemplateList = ({
  templates = [],
  error,
  deleteTemplate,
}) => {
  const [filter, setFilter] = useState('')
  const filteredTemplates = useMemo(() => templates.filter(item => (
    transformText(item.name).includes(transformText(filter))
  )), [filter, templates])

  const transformText = text => text.toLowerCase().trim()

  const formatTime = date => {
    const addZero = val => val.length === 1 ? `0${val}` : val
    const d = new Date(date)
    const year = d.getFullYear().toString()
    const month = (d.getMonth() + 1).toString()
    const day = d.getDate().toString()
    return `${addZero(day)}-${addZero(month)}-${year}`
  }

  const handleChangeFilter = event => {
    setFilter(event.target.value)
  }

  return (
    <>
      <h2 className='mb-5 mt-3'>Fragenliste Templates</h2>
      <div className='row mb-5'>
        <div className='col-8'>
          <input
            className='form-control'
            type='text'
            placeholder='Type to filter'
            onChange={handleChangeFilter}
          />
        </div>
        <div className='col-4 d-flex justify-content-end'>
          <Link
            to='/create-template'
            className='btn btn-primary'
          >
            Create new
          </Link>
        </div>
      </div>
      <table className='table table-striped'>
        <thead>
        <tr>
          <th>id</th>
          <th>name</th>
          <th>created</th>
          <th>updated</th>
          <th>default</th>
          <th />
          {/*<th />*/}
        </tr>
        </thead>
        <tbody>
        {error && (<>
          <tr />
          <tr>
            <td colSpan={6}>Error on templates loading</td>
          </tr>
        </>)}
        {!filteredTemplates.length && !error && (<>
          <tr />
          <tr>
            <td colSpan={6}>No templates found</td>
          </tr>
        </>)}
        {filteredTemplates.map(({
          id,
          name,
          created_at: createdAt,
          updated_at: updatedAt,
          is_default: isDefault,
        }) => (
          <tr key={id}>
            <td>{id}</td>
            <td>{name}</td>
            <td>{formatTime(createdAt)}</td>
            <td>{formatTime(updatedAt)}</td>
            <td>
              <input
                type='checkbox'
                checked={isDefault}
                disabled={true}
              />
            </td>
            <td className='d-flex justify-content-end'>
              <Link
                to={`/template/${id}`}
                className='btn btn-sm'
              >
                <FontAwesomeIcon
                  icon={faPencilAlt}
                  className='text-primary'
                />
              </Link>
              {/*<button className='btn btn-sm' onClick={() => deleteTemplate(id)}>*/}
              {/*  <FontAwesomeIcon*/}
              {/*    icon={faTrashAlt}*/}
              {/*    className='text-danger'*/}
              {/*  />*/}
              {/*</button>*/}
            </td>
          </tr>
        ))}
        </tbody>
      </table>
    </>
  )
}
