import React, { Component } from 'react'

export class ErrorBoundary extends Component {
  state = {
    error: false,
  }

  static getDerivedStateFromError() {
    return { error: true }
  }

  render() {
    const { children } = this.props
    const { error } = this.state

    if (error) {
      return (
        <div>Something went wrong. Please try to refresh the page.</div>
      )
    }

    return children
  }
}
