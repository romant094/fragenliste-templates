export class ApiClient {
  constructor(apiBase) {
    this.headers = {
      'Content-Type': 'application/json',
    }
    this._apiBase = apiBase
  }

  fetchCustom = async (url, options) => {
    const res = await fetch(url,options)
    if (!res.ok) {
      console.log(res)
      const json = await res.json()
      console.log(json)
      throw Error(`ERROR FUCK YOU =====> ${json}`)
    }
    return await res.json()
  }

  get = async ({ endpoint, params = {} }) => {
    try {
      const headers = { ...this.headers, ...params?.headers }
      const res = await fetch(`${this._apiBase}${endpoint}`, {
        method: 'GET',
        headers,
        ...params,
      })
      if (!res.ok) {
        throw Error(res.error)
      }
      return await res.json()
    } catch (e) {
      console.error(`Error text: ${e}`)
    }
  }

  post = async ({ endpoint, data, params }) => {
    try {
      const res = await fetch(`${this._apiBase}${endpoint}/`, {
        method: 'POST',
        headers: this.headers,
        body: JSON.stringify(data),
        ...params,
      })
      const json = await res.json()
      if (!res.ok) {
        throw Error(json.error)
      }
      return json
    } catch (e) {
      console.error(`Error text: ${e}`)
    }
  }

  delete = async ({ endpoint, params }) => {
    try {
      const res = await fetch(`${this._apiBase}${endpoint}/`, {
        method: 'DELETE',
        headers: this.headers,
        ...params,
      })
      const json = await res.json()
      if (!res.ok) {
        throw Error(json.error)
      }
      return json
    } catch (e) {
      console.error(`Error text: ${e}`)
    }
  }

  patch = async ({ endpoint, data, params }) => {
    return this.post({
      endpoint,
      data,
      params: {
        ...params,
        method: 'PATCH',
      },
    })
  }
}
