import { ApiClient } from './ApiClient'

const {REACT_APP_AUTH_ENDPOINT, REACT_APP_FRAGEN_ENDPOINT} = process.env

export const api = new ApiClient(`${REACT_APP_FRAGEN_ENDPOINT}/api`)
export const authApi = new ApiClient(REACT_APP_AUTH_ENDPOINT)
